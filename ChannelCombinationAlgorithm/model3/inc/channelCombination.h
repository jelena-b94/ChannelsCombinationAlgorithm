void channel_combination(__memY DSPfract* inBufLeft, __memY DSPfract* outBufL, __memY DSPfract* outBufC, __memY DSPfract* inBufRight,
		__memY DSPfract* outBufR, __memY DSPfract* outBufLs, __memY DSPfract* outBufRs, __memY DSPfract* outBufLFE);
