#ifndef _COMMON_H
#define _COMMON_H

#include "stdfix_emu.h"

/* Basic constants */
#define BLOCK_SIZE 16
#define MAX_NUM_CHANNEL 8
#define BUFFER_MAX_LENGTH 10
#define N_DYNAMIC_LOSS 3
#define N_STATIC_LOSS 4
#define N_COEFFS 32
#define OUTPUTS_NUM 10


/* DSP type definitions */
typedef short DSPshort;					/* DSP integer */
typedef unsigned short DSPushort;		/* DSP unsigned integer */
typedef int DSPint;						/* native integer */
typedef fract DSPfract;				    /* DSP fixed-point fractional */
typedef long_accum DSPaccum;			/* DSP fixed-point fractional */

#endif //_COMMON_H
