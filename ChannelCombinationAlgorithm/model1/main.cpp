/////////////////////////////////////////////////////////////////////////////////
//
// @file model0.cpp
//
// Module: channelsCombinationAlgorithm
// Description:  
// $Source: $
// $Revision: 1.0 $
// $Date: 3.10.2017 $
// $Author: Jelena Banjac $
//
/////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h>
#include <math.h> 
#include "WAVheader.h"
#include "common.h"

/////////////////////////////////////////////////////////////////////////////////
// IO buffers
/////////////////////////////////////////////////////////////////////////////////
double sampleBuffer[MAX_NUM_CHANNEL][BLOCK_SIZE];
double output[10];
/*const double coeffs[N_COEFFS] = {0.000281, 0.000557, 0.000934, 0.001422, 0.002031, 0.002764, 0.003620, 0.004592, 0.005669, 0.006831, 
								0.008055, 0.009313, 0.010572, 0.011798, 0.012955, 0.014006, 0.014919, 0.015664, 0.016214, 0.016553, 
								0.016667, 0.016553, 0.016214, 0.015664, 0.014919, 0.014006, 0.012955, 0.011798, 0.010572, 0.009313, 
								0.008055, 0.006831, 0.005669, 0.004592, 0.003620, 0.002764, 0.002031, 0.001422, 0.000934, 0.000557, 
								0.000281}; */
const double coeffs[N_COEFFS] = {0.000084, 0.000163, 0.000269, 0.000404, 0.000568, 0.000763, 0.000986, 0.001236, 0.001509, 0.001800, 
								0.002103, 0.002411, 0.002717, 0.003012, 0.003289, 0.003539,0.003756, 0.003931, 0.004061, 0.004140, 
								0.004167, 0.004140, 0.004061, 0.003931, 0.003756, 0.003539, 0.003289, 0.003012, 0.002717, 0.002411, 
								0.002103, 0.001800, 0.001509, 0.001236, 0.000986, 0.000763, 0.000568, 0.000404, 0.000269, 0.000163, 
								0.000084};
/////////////////////////////////////////////////////////////////////////////////

double history;
int p_state;


/////////////////////////////////////////////////////////////////////////////////
// Output Mode
/////////////////////////////////////////////////////////////////////////////////
typedef enum  
{
	OUTPUT_MODE_3_2_0 = 0,	//LR, C, LsRs
	OUTPUT_MODE_2_0_0,		//LR  
	OUTPUT_MODE_2_0_1,		//LR, LFE
	OUTPUT_MODE_3_2_1		//LR c, LsRs, LFE
} OutputMode;
/////////////////////////////////////////////////////////////////////////////////

OutputMode outputMode;

/////////////////////////////////////////////////////////////////////////////////
// Control state structure
/////////////////////////////////////////////////////////////////////////////////
typedef struct  
{
	double* pChannelCombinationBuff;
	int bufferLength;
	double dynamic_loss[N_DYNAMIC_LOSS];
	int n_dynamic_loss;
	double static_loss[N_STATIC_LOSS];
	int n_static_loss;
} ChannelCombinationState;

typedef struct  
{
	double* pInBufLeft;		// System inputs
	double* pInBufRight;	
	double* pOutBufLs;		// System outputs
	double* pOutBufL;
	double* pOutBufC;
	double* pOutBufR;
	double* pOutBufRs;
	double* pOutBufLFE;
	int bufferLength;		// Lenght of these samples in inputn (and output as well)
} ChannelCombination;
/////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// Channel combination loss state and initialization constants
/////////////////////////////////////////////////////////////////////////////////
ChannelCombinationState channelCombinationState;
ChannelCombination channelCombination;
double channel_combination_buffer[BUFFER_MAX_LENGTH];
double dynamic_loss[N_DYNAMIC_LOSS];					// Dynamic loss/gain values
const double static_loss[N_STATIC_LOSS] = {				// Static loss/gain values
	0.7943282347242815, 0.7079457843841379, 0.6309573444801932, 0.8912509381337456	// -2dB, -3dB, -4dB, -1dB
}; 
	

double fir_circular(double input, double *history, int *p_state) 
{
	int i;
	int state;
	double ret_val;
	
	state = *p_state;
	history[state] = input;
	if (++state >= N_COEFFS)
	{
		state = 0;
	}
	ret_val = 0.0;
	for (i = N_COEFFS - 1; i >= 0; i --)
	{
		ret_val += coeffs[i] * history[state];
		if (++state >= N_COEFFS)
		{
			state = 0;
		}
	}

	*p_state = state;

	return ret_val;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// multitap_echo_init
//
// @param - echoState - Control state structure
//		  - buffer - buffer for keeping delayed samples
//		  - echoBufLen - Length of buffer
//		  - delay - array containing delay values in number of samples
//		  - input_gain - gain to be applayed to input sample
//		  - tap_gain - array of gains to be applayed to each delayed sample
//		  - n_tap - number of taps (equals length of delay and tap_gain)
//
// @return - nothing
// Comment: Initialize echoState structure
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination_state_init()
{
	int i;
	double *pInitChannelCombinationBuffer = channel_combination_buffer;
	for (i=0; i<BUFFER_MAX_LENGTH; i++) 
	{
		*pInitChannelCombinationBuffer = 0.0;
		pInitChannelCombinationBuffer++;
	}
	channelCombinationState.pChannelCombinationBuff = channel_combination_buffer;
	channelCombinationState.bufferLength = BUFFER_MAX_LENGTH;

	double *pChannelCombinationDynamicLoss = channelCombinationState.dynamic_loss;
	double *pInitChannelCombinationDynamicLoss = dynamic_loss;
	for (i=0; i<N_DYNAMIC_LOSS; i++) 
	{
		*(pChannelCombinationDynamicLoss++) = *(pInitChannelCombinationDynamicLoss++);
	}
	channelCombinationState.n_dynamic_loss = N_DYNAMIC_LOSS;

	double *pChannelCombinationStaticLoss = channelCombinationState.static_loss;
	const double *pInitChannelCombinationStaticLoss = static_loss;
	for (i=0; i<N_STATIC_LOSS; i++) 
	{
		*(pChannelCombinationStaticLoss++) = *(pInitChannelCombinationStaticLoss++);
	}
	channelCombinationState.n_static_loss = N_STATIC_LOSS;
}

//void channel_combination_init(ChannelCombination* channelCombination, double* inBufLeft, double* outBufC, double* inBufRight, double* outBufLs, double* outBufRs, double* outBufLFE, int buffLen)
void channel_combination_init()
{
	channelCombination.pInBufLeft = sampleBuffer[0];
	channelCombination.pInBufRight = sampleBuffer[2];
	channelCombination.pOutBufLs = sampleBuffer[3];
	channelCombination.pOutBufL = sampleBuffer[0];
	channelCombination.pOutBufC = sampleBuffer[1];
	channelCombination.pOutBufR = sampleBuffer[2];
	channelCombination.pOutBufRs = sampleBuffer[4];
	channelCombination.pOutBufLFE = sampleBuffer[5];
	channelCombination.bufferLength = BLOCK_SIZE;
}


/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017 
//
// Function:
// multitap_echo
//
// @param - pInbuf - Buffer with input samples
//		  - pOutbuf - Buffer with output samples
//		  - inputLen - Length of input and output buffer
//		  - echoState - Control state structure
//
// @return - nothing
// Comment: Apply echo to input samples
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination(double* inBufLeft, double* outBufL, double* outBufC, double* inBufRight,
		double* outBufR, double* outBufLs, double* outBufRs,double* outBufLFE)
{
	int i, j;
	double *out = output;
	double *pDynamicLoss = channelCombinationState.dynamic_loss;
	double *pStaticLoss = channelCombinationState.static_loss;
	/*double *pInBufLeft = channelCombination.pInBufLeft;
	double *pInBufRight = channelCombination.pInBufRight;
	double *pOutBufLs = channelCombination.pOutBufLs;
	double *pOutBufL = channelCombination.pOutBufL;
	double *pOutBufC = channelCombination.pOutBufC;
	double *pOutBufR = channelCombination.pOutBufR;
	double *pOutBufRs = channelCombination.pOutBufRs;
	double *pOutBufLFE = channelCombination.pOutBufLFE;*/
	/*double *pInBufLeft = sampleBuffer[0];
	double *pInBufRight = sampleBuffer[2];
	double *pOutBufLs = sampleBuffer[3];
	double *pOutBufL = sampleBuffer[0];
	double *pOutBufC = sampleBuffer[1];
	double *pOutBufR = sampleBuffer[2];
	double *pOutBufRs = sampleBuffer[4];
	double *pOutBufLFE = sampleBuffer[5];*/
	double *pInBufLeft = inBufLeft;
	double *pInBufRight = inBufRight;
	double *pOutBufLs = outBufLs;
	double *pOutBufL = outBufL;
	double *pOutBufC = outBufC;
	double *pOutBufR = outBufR;
	double *pOutBufRs = outBufRs;
	double *pOutBufLFE = outBufLFE;

	for(i = 0; i < BLOCK_SIZE; i++)
	{		
		*(out++) = (*(pInBufLeft++)) * (*(pDynamicLoss++));			// state 0
		*(out++) = (*(pInBufRight++)) * (*(pDynamicLoss++));		// state 1
		*(out++) = *(out-2) + *(out-1);						// state 2
		*(out++) = *(out-1) * (*pDynamicLoss);				// state 3
		
		switch (outputMode) 
		{
		case OUTPUT_MODE_3_2_0:		// L R C Ls Rs
		case OUTPUT_MODE_3_2_1:		// L R C Ls Rs LFE
			*(out++) = *(out-4) * (*(pStaticLoss++));		// state 4
			*(out++) = *(out-2) * (*(pStaticLoss++));		// state 5
			*(out++) = *(out-3) * (*(pStaticLoss++));		// state 6
			*(out++) = *(out-6) * (*(pStaticLoss));			// state 7
			*(out++) = *(out-4) + *(out-3);				// state 8
			*out = *(out-3) + *(out-2);					// state 9

			out -= 9;	// Restart
			pStaticLoss -= 3;
			pDynamicLoss -= 2;

			*(pOutBufLs++) = *(out+8);	// Ls buffer output	
			*(pOutBufL++) = *(out+5);	// L buffer output			
			*pOutBufC = *(out+3);	// C buffer output			
			*(pOutBufR++) = *(out+6);	// R buffer output
			*(pOutBufRs++) = *(out+9);	// Rs buffer output

			if (OUTPUT_MODE_3_2_1) 
			{
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state);	// LFE buffer output
				*(pOutBufC++);
			}
			else 
			{
				*(pOutBufC++);
				*(pOutBufLFE++) = 0.0;	// LFE buffer output
			}
			break;
		case OUTPUT_MODE_2_0_0:		// L R
		case OUTPUT_MODE_2_0_1:		// L R LFE
			*(out++) = *(out-1) * (*(pStaticLoss+1));		// state 5 (in place of state 4)
			*(out) = *(out-2) * (*(pStaticLoss+2));		// state 6 (in place of state 5)

			out -= 5;	// Restart
			pDynamicLoss -= 2;

			*(pOutBufLs++) = 0.0;		// Ls buffer output	
			*(pOutBufL++) = *(out+4);	// L buffer output					
			*(pOutBufR++) = *(out+5);	// R buffer output
			*(pOutBufRs++) = 0.0;		// Rs buffer output

			if (OUTPUT_MODE_2_0_1) 
			{
				*pOutBufC = *(out+3);	// C buffer output		
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state); // LFE buffer output
				*(pOutBufC++);
			}
			else 
			{
				*(pOutBufC++) = 0.0;		// C buffer output		
				*(pOutBufLFE++) = 0.0;	// LFE buffer output
			}
			break;
		}

	}
	//p_state = 0;
	//pInBufLeft = pInBufLeft-16;
	//pInBufRight = pInBufRight-16;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// main
//
// @param - argv[1] - Input file name
//        - argv[2] - Output file name
//		  - argv[3] - Input gain left
//		  - argv[4] - Input gain right
//		  - argv[5] - Headroom gain
// @return - nothing
// Comment: main routine of a program
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	FILE *wav_in=NULL;
	FILE *wav_out=NULL;
	char WavInputName[256];
	char WavOutputName[256];
	WAV_HEADER inputWAVhdr,outputWAVhdr;	

	int i;

	// Dynamic loss/gain values
	double *pDynamicLoss = dynamic_loss;
	for (i=0; i<N_DYNAMIC_LOSS; i++)
	{
		*(pDynamicLoss++) = pow(10, atoi(argv[i+3])/20.0);	// Two input gains (args 3,4) and one headroom gain (arg 5)
	}

	outputMode = (OutputMode)atoi(argv[6]);					// Output mode (arg 6)
	
	// Init channel buffers
	for(i=0; i<MAX_NUM_CHANNEL; i++)
		memset(&sampleBuffer[i],0,BLOCK_SIZE);

	// Open input and output wav files
	//-------------------------------------------------
	strcpy(WavInputName,argv[1]);
	wav_in = OpenWavFileForRead (WavInputName,"rb");
	strcpy(WavOutputName,argv[2]);
	wav_out = OpenWavFileForRead (WavOutputName,"wb");
	//-------------------------------------------------

	// Read input wav header
	//-------------------------------------------------
	ReadWavHeader(wav_in,inputWAVhdr);
	//-------------------------------------------------
	
	// Set up output WAV header
	//-------------------------------------------------	
	outputWAVhdr = inputWAVhdr;
	outputWAVhdr.fmt.NumChannels = 6; // change number of channels

	int oneChannelSubChunk2Size = inputWAVhdr.data.SubChunk2Size/inputWAVhdr.fmt.NumChannels;
	int oneChannelByteRate = inputWAVhdr.fmt.ByteRate/inputWAVhdr.fmt.NumChannels;
	int oneChannelBlockAlign = inputWAVhdr.fmt.BlockAlign/inputWAVhdr.fmt.NumChannels;
	
	outputWAVhdr.data.SubChunk2Size = oneChannelSubChunk2Size*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.ByteRate = oneChannelByteRate*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.BlockAlign = oneChannelBlockAlign*outputWAVhdr.fmt.NumChannels;


	// Write output WAV header to file
	//-------------------------------------------------
	WriteWavHeader(wav_out,outputWAVhdr);

	
	// Initialize echo 
	channel_combination_state_init();
	

	// Processing loop
	//-------------------------------------------------	
	{
		int sample;
		int BytesPerSample = inputWAVhdr.fmt.BitsPerSample/8;
		const double SAMPLE_SCALE = -(double)(1 << 31);		//2^31
		int iNumSamples = inputWAVhdr.data.SubChunk2Size/(inputWAVhdr.fmt.NumChannels*inputWAVhdr.fmt.BitsPerSample/8);
		
		// exact file length should be handled correctly...
		for(int i=0; i<iNumSamples/BLOCK_SIZE; i++)
		{	
			for(int j=0; j<BLOCK_SIZE; j++)
			{
				for(int k=0; k<inputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = 0; //debug
					fread(&sample, BytesPerSample, 1, wav_in);
					sample = sample << (32 - inputWAVhdr.fmt.BitsPerSample); // force signextend
					sampleBuffer[k][j] = sample / SAMPLE_SCALE;				// scale sample to 1.0/-1.0 range		
				}
			}
			//channel_combination_init();
			
			// Call processing on first channel
			channel_combination(sampleBuffer[0],sampleBuffer[0],sampleBuffer[1],sampleBuffer[2],sampleBuffer[2],sampleBuffer[3],sampleBuffer[4],sampleBuffer[5]);
			
			for(int j=0; j<BLOCK_SIZE; j++)
			{
				for(int k=0; k<outputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = sampleBuffer[k][j] * SAMPLE_SCALE ;	// crude, non-rounding 			
					sample = sample >> (32 - inputWAVhdr.fmt.BitsPerSample);
					fwrite(&sample, outputWAVhdr.fmt.BitsPerSample/8, 1, wav_out);		
				}
			}		
		}
	}
	
	// Close files
	//-------------------------------------------------	
	fclose(wav_in);
	fclose(wav_out);
	//-------------------------------------------------	

	return 0;
}