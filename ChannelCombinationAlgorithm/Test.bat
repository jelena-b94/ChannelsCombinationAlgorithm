: Delete log files first.
cd OutCmp
del whiteNoise_Model1_vs_Model2.txt
del whiteNoise_Model1_vs_Model2.txt
del whiteNoise_Model2_vs_Model3.txt

cd ..

: Execute Model 0, Model 1, Model 2 and Model 3
cd Debug
"model0.exe" "..//..//TestStreams//WhiteNoise.wav" "..//OutStreams//WhiteNoise_model0.wav" -6 -6 -6 3

:: TO DO: Call model 1 executable and name output file: WhiteNoise_model1.wav
"model1.exe" "..//..//TestStreams//WhiteNoise.wav" "..//OutStreams//WhiteNoise_model1.wav" -6 -6 -6 3

:: TO DO: Call model 2 executable and name output file: WhiteNoise_model2.wav
"model2.exe" "..//..//TestStreams//WhiteNoise.wav" "..//OutStreams//WhiteNoise_model2.wav" -6 -6 -6 3

:"model3.exe" "..//..//TestStreams//WhiteNoise.wav" "..//OutStreams//WhiteNoise_model3.wav" -6 -6 -6 3

cd ..

: Generate new logs
"..//tools//PCMCompare.exe" OutStreams//WhiteNoise_model0.wav OutStreams//WhiteNoise_model1.wav 2> OutCmp//whiteNoise_Model0_vs_Model1.txt

:: TO DO: Compare output of model1 and model2 and store the result in OutCmp//whiteNoise_Model1_vs_Model2.txt
"..//tools//PCMCompare.exe" OutStreams//WhiteNoise_model1.wav OutStreams//WhiteNoise_model2.wav 2> OutCmp//whiteNoise_Model1_vs_Model2.txt

"..//tools//PCMCompare.exe" OutStreams//WhiteNoise_model2.wav OutStreams//WhiteNoise_model3.wav 2> OutCmp//whiteNoise_Model2_vs_Model3.txt


